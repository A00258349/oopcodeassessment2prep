package com.example.threads;

import static java.lang.Thread.sleep;

// Implementing Runnable does not work (contrary to a StackOverflow solution!)
// http://stackoverflow.com/questions/6546193/how-to-catch-an-exception-from-a-thread

/* Output

main - finishing...
TheProblemAgain::run()
Exception in thread "TheProblemAgain" java.lang.RuntimeException: TheProblemAgain::RuntimeException
	at threading.runtime_exceptions.TheProblemAgain.run(TheProblemAgain.java:40)
	at java.lang.Thread.run(Thread.java:744)

*/
public class TheProblemAgain implements Runnable {

    public static void main(String[] args) {
        TheProblemAgain otherThread = new TheProblemAgain();
        try {
            new Thread(otherThread).start();
        } catch (RuntimeException e) {
            // this handler is never executed - this is THE PROBLEM!
            System.out.println(Thread.currentThread().getName() + " - caught the RuntimeException generated in the thread"); // not printed!!
        }
        System.out.println(Thread.currentThread().getName() + " - finishing...");
    }

    @Override
    public void run() {
        Thread.currentThread().setName("TheProblemAgain");
//    try {
        while (true) {
            System.out.println(Thread.currentThread().getName() + "::run()");

            try {
                sleep(2000);
            } catch (InterruptedException e) {
            }
            // The exception thrown on the next line is uncaught and terminates the thread
            throw new RuntimeException(Thread.currentThread().getName() + "::RuntimeException");
        }
//    } catch (RuntimeException rte) {
//      System.out.println("run():: RTException in thread");
//      throw rte;
//    } 
    }
}
