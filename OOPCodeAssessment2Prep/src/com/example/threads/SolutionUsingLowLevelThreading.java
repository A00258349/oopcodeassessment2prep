package com.example.threads;

/* Output:

RTE Thread sleeping ...
RTE Thread throwing exception ...
Handler for uncaught exception: java.lang.RuntimeException: RTE Thread

 */

// Based on the API for UncaughtExceptionHandler:
public class SolutionUsingLowLevelThreading {

    public static void main(String[] args) {
        // Define the UncaughtExceptionHandler
        Thread.UncaughtExceptionHandler uchl = new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread th, Throwable ex) {
                System.out.println("Handler for uncaught exception: " + ex);
            }
        };
        // Define the Runnable
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+" sleeping ...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("Interrupted.");
                }
                System.out.println(Thread.currentThread().getName() + " throwing exception ...");
                throw new RuntimeException(Thread.currentThread().getName());
            }
        };
        // Create the thread, passing in the Runnable (job to be done)
        Thread thread = new Thread(runnable);
        // Give the thread a name
        thread.setName("RTE Thread");
        // set the threads UncaughtExceptionHandler
        thread.setUncaughtExceptionHandler(uchl);
        // start the thread
        thread.start();
        try {
            thread.join(); // force the main thread to wait for the RTE thread to complete
        } catch (InterruptedException ex) {
            System.out.println("Interrupted.");
        }
    }
}
