package com.example.threads;
// A RuntimeException thrown in a thread is not visible to the "main" thread.
// Based on http://stackoverflow.com/questions/6546193/how-to-catch-an-exception-from-a-thread


/* Output

 theProblemThread::run()
 main - finishing...
 Exception in thread "theProblemThread" java.lang.RuntimeException: theProblemThread::RuntimeException
 at threading.runtime_exceptions.TheProblem.run(TheProblem.java:41)

 */
public class TheProblem extends Thread {

    public static void main(String[] args) throws InterruptedException {
        TheProblem theProblemThread = new TheProblem();
        theProblemThread.setName("theProblemThread");

        try {
            theProblemThread.start();
            theProblemThread.join(); // wait for 'theProblemThread' to finish (throws InterruptedException)
        } catch (RuntimeException e) {
            // this handler is never executed - this is THE PROBLEM!
            System.out.println(Thread.currentThread().getName() + " - caught the RuntimeException generated in the thread"); // not printed!!
        }
        System.out.println(Thread.currentThread().getName() + " - finishing...");
    }

    // Note that run() doe NOT thow an Exception (a Callable can)
    // So, I can never throw a checked exception from run() - I can only throw RuntimeException's
    @Override
    public void run() {
//        try {      // if I wanted to catch and handle the RuntimeException inside run()
            while (true) {
                System.out.println(Thread.currentThread().getName() + "::run()");

                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                }
                // The exception thrown on the next line is uncaught and terminates the thread
                throw new RuntimeException(Thread.currentThread().getName() + "::RuntimeException");

//                throw new Exception(Thread.currentThread().getName() + "::Exception");
            }
        
        // Can handle the RTE and re-throw it (choosing not to in this instance)
//        } catch (RuntimeException rte) {
//           System.out.println(Thread.currentThread().getName() + " caught RuntimeException!!");
//            throw rte;  // throw it out to main() thread
//        }
        
//        } catch (Exception e) {
//            System.out.println(Thread.currentThread().getName() + " caught Exception!!");
//        }
    }
}
