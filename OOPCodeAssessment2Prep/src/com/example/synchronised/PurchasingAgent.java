package com.example.synchronised;

public class PurchasingAgent {
	public PurchasingAgent() {
		System.err.println("Purchasing agent created");
	}
	public void purchase() {
		try {
			Thread.sleep((int) Math.random()*3000+1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Store store = Store.getInstance();
		synchronized(store) {
			if(store.getShirtCount() != 0 && store.authorizeCreditCard("1234", 15.00)) {
				store.takeShirt();
				System.out.println(Thread.currentThread().getName() + " took shirt");
			} else {
				System.out.println(Thread.currentThread().getName() + " failed to get shirt");
			}
		}
		
	}
}
