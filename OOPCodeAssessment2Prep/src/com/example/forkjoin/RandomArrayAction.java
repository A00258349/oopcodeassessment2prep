package com.example.forkjoin;

import java.util.concurrent.RecursiveAction;
import java.util.concurrent.ThreadLocalRandom;

public class RandomArrayAction extends RecursiveAction {
	
	private static final long serialVersionUID = 1L;
	private int[] myArray;
	private int start;
	private int end;
	private int threshold;
	
	public RandomArrayAction(int[] myArray, int start, int end, int threshold) {
		this.myArray = myArray;
		this.start = start;
		this.end = end;
		this.threshold = threshold;
	}

	@Override
	protected void compute() {
		if(end - start < threshold) {
			for(int i = start; i <= end; i++) {
				myArray[i] = ThreadLocalRandom.current().nextInt();
			}
		} else {
			int midway = (end - start) / 2 + start;
			RandomArrayAction lowerHalf = new RandomArrayAction(myArray, start, midway, threshold);
			RandomArrayAction upperHalf = new RandomArrayAction(myArray, midway+1, end, threshold);
			invokeAll(lowerHalf, upperHalf);
		}
	}
}
