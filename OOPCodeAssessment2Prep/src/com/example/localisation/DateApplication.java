package com.example.localisation;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class DateApplication {

	PrintWriter pw = new PrintWriter(System.out, true);
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	Locale ruLocale = new Locale("ru", "RU");
	// Locale currentLocale = Locale.US;
	Locale irlLocale = new Locale("en", "IE");
	Locale currentLocale = irlLocale; // start out with en_IE (i.e. English in Ireland) menu
	ResourceBundle messages = ResourceBundle.getBundle("MessagesBundle", currentLocale);

	public static void main(String[] args) {
		DateApplication dateApp = new DateApplication();
		dateApp.run();
		// dateApp.java8();
	}

	public void java8() {
		LocalDate startWW2 = LocalDate.of(1939, Month.SEPTEMBER, 1);
		LocalDate endWW2 = LocalDate.of(1945, Month.SEPTEMBER, 1);
		System.out.println("WW2 started on: " + startWW2.getDayOfWeek());
		System.out.println("WW2 ended on: " + endWW2.getDayOfWeek());

		LocalDate now = LocalDate.now();
		LocalDate eighteenYearsAgo = now.minus(18, ChronoUnit.YEARS);
		LocalDate joeBloggsBirthday = LocalDate.of(2000, Month.FEBRUARY, 12);
		LocalDate annBloggsBirthday = LocalDate.of(2010, Month.DECEMBER, 20);

		System.out.println("To be served, you must be born before : " + eighteenYearsAgo);
		System.out.println("Joe Bloggs DOB is " + joeBloggsBirthday);

		if (joeBloggsBirthday.isBefore(eighteenYearsAgo)) {
			System.out.println("Yes. Joe is old enough to be served alcohol.");
		} else {
			System.out.println("No. Joe is too young to be served alcohol.");
		}
		System.out.println("Ann Bloggs DOB is " + annBloggsBirthday);
		if (annBloggsBirthday.isBefore(eighteenYearsAgo)) {
			System.out.println("Yes. Ann is old enough to be served alcohol.");
		} else {
			System.out.println("No. Ann is too young to be served alcohol.");
		}

		LocalTime sundayRise = LocalTime.of(06, 30).plus(1, ChronoUnit.HOURS);
		System.out.println("I get up on Sunday at: " + sundayRise);

		LocalDateTime nowDT = LocalDateTime.now();
		// Let's get the local time in Tripoli
		ZonedDateTime athloneZoneTime = nowDT.atZone(ZoneId.of("Europe/Dublin"));
		ZonedDateTime tripoliZoneTime = athloneZoneTime.withZoneSameInstant(ZoneId.of("Africa/Tripoli"));
		// In Athlone it is: 2019-03-20T20:39:22.876Z[Europe/Dublin]
		// In Tripoli it is: 2019-03-20T22:39:22.876+02:00[Africa/Tripoli]
		System.out.println("In Athlone it is: " + athloneZoneTime);
		System.out.println("In Tripoli it is: " + tripoliZoneTime);

		String dateTimeFormatPattern = "yyyy/MM/dd HH:mm:ss z";
		String dateTimeFormatPattern2 = "yyyy/MM/dd";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormatPattern);
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern(dateTimeFormatPattern2);
		ZonedDateTime nowZone = ZonedDateTime.now();
		System.out.println(nowZone + " formatted with DateTimeFormatter and '" + dateTimeFormatPattern + "': "
				+ formatter.format(nowZone));
		System.out.println(nowZone + " formatted with DateTimeFormatter and '" + dateTimeFormatPattern2 + "': "
				+ formatter2.format(nowZone));

	}

	public void run() {
		String line = "";

		while (!(line.equals("q"))) {
			this.printMenu();
			try {
				line = this.br.readLine();
			} catch (Exception e) {
				e.printStackTrace();
			}

			switch (line) {
			case "1":
				setEnglish();
				break;
			case "2":
				setFrench();
				break;
			case "3":
				setChinese();
				break;
			case "4":
				setRussian();
				break;
			}
		}
	}
	DateFormat df;
	SimpleDateFormat sdf;
	public void printMenu() {
		pw.println("=== Date App ===");
		// As opposed to lots of formatting (as in the Java 7 lab), I am focusing on
		// the new Java 8 material (java.time packages). There are some format q's
		// still in this lab; plus formatting in also the Notes (slide 13).

		df = DateFormat.getDateInstance(DateFormat.DEFAULT, currentLocale);
		pw.print("\n"+messages.getString("date1")+" "+df.format(new Date()));
		
		df = DateFormat.getDateInstance(DateFormat.LONG, currentLocale);
		pw.print("\n"+messages.getString("date2")+" "+df.format(new Date()));
		
		df = DateFormat.getDateInstance(DateFormat.SHORT, currentLocale);
		pw.print("\n"+messages.getString("date3")+" "+df.format(new Date()));
		
		df = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
		pw.print("\n"+messages.getString("date4")+" "+df.format(new Date()));
		
		df = DateFormat.getTimeInstance(DateFormat.FULL, currentLocale);
		pw.print("\n"+messages.getString("date5")+" "+df.format(new Date()));
		
		sdf = new SimpleDateFormat("EEEE", currentLocale);
		pw.println("\n"+messages.getString("date6") +" "+sdf.format(new Date()));
		
		sdf = new SimpleDateFormat("EEEE MMMM d, y G kk:mm:ss", currentLocale);
		pw.println("\n"+messages.getString("date7") +" "+sdf.format(new Date()));
		
		pw.println("\n--- Choose Language Option ---");
		pw.println("1. " + messages.getString("menu1"));
		pw.println("2. " + messages.getString("menu2"));
		pw.println("3. " + messages.getString("menu3"));
		pw.println("4. " + messages.getString("menu4"));
		pw.println("q. " + messages.getString("menuq"));
		System.out.print(messages.getString("menucommand") + " ");

	}

	public void setEnglish() {
		currentLocale = Locale.US;
		messages = ResourceBundle.getBundle("MessagesBundle", currentLocale);
	}

	public void setFrench() {
		currentLocale = Locale.FRANCE;
		messages = ResourceBundle.getBundle("MessagesBundle_fr_FR", currentLocale);
	}

	public void setChinese() {
		currentLocale = Locale.CHINA;
		messages = ResourceBundle.getBundle("MessagesBundle_zh_CN", currentLocale);
	}

	public void setRussian() {
		currentLocale = new Locale("ru", "RU");
		messages = ResourceBundle.getBundle("MessagesBundle_ru_RU", currentLocale);
	}
}
